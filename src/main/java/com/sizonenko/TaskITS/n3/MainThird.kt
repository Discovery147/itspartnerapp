package com.sizonenko.TaskITS.n3

/**
 * Description: Application calculates sum of ages of all users and finds the eldest user
 * Task: Refactor, use Kotlin style constructions. Calculate average age of users.
 */
@SuppressWarnings("all")
fun main(args: Array<String>) {

    val u = mutableListOf<User>(User("Pavel", 50), User("Suresh", 50), User("Robin", 50))

    val s = u.sumBy { user -> user.age }
    val max = u.maxBy { user -> user.age }

    println("The eldest user is " + max?.name)
    println("Sum of all ages is " + s)
    println("Average age is " + s / u.size)

}
