package com.sizonenko.TaskITS.n4;

import org.junit.*;

public class MainFourthTest {

    private static BiggestNumberFinder numberFinder;

    @BeforeClass
    public static void init() {
        numberFinder = new MainFourth();
    }

    @Test
    public void findTheBiggestNumberTest() {
        int[] numbers = {-58, 0, 33, -1, 99, -39};
        int expected = 99;
        int actual = numberFinder.findTheBiggestNumber(numbers);
        Assert.assertEquals(expected, actual);
    }
}
