package com.sizonenko.TaskITS.n6;

import java.io.IOException;
import java.util.*;

/**
 * Task: Process file resources/n6/source in a special way and write result to the new file.
 * Source file contains data in following format:
 * username,password,email
 * <p>
 * You need to:
 * 1) remove duplicates by username
 * 2) make email lowercase
 * 3) remove records where password doesn't meet following conditions:
 * 3.1) password length must be at least 8 symbols
 * 3.2) password must contain at least one special character (#, @, -)
 * 3.3) password must contain exactly one uppercase character
 * <p>
 * All the rest records should be saved in new file in the same format.
 */
public class MainSixth {

    private static final String INPUT = "sixth/source";
    private static final String OUTPUT = "output";

    public static void main(String[] args) {
        FileManager fileManager = new FileManager();
        UserAction userAction = new UserAction();
        try {
            List<User> users = DataParser.parseDataToUser(fileManager.readData(INPUT));

            users = userAction.removeDublicates(users);
            userAction.makeLowercaseEmail(users);
            userAction.removeByPassword(users);

            fileManager.writeData(DataParser.parseUsersToStrng(users), OUTPUT);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("File not found");
        }
    }
}

