package com.sizonenko.TaskITS.n6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileManager {
    public List<String> readData(String path) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<String> lines = new ArrayList<>();
        while (reader.readLine() != null) {
            lines.add(reader.readLine());
        }
        return lines;
    }

    public void writeData(List<String> users, String path) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            for (String user : users) {
                bw.write(user + System.lineSeparator());
            }
        }
    }
}
