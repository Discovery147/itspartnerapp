package com.sizonenko.TaskITS.n7;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Write simple spring boot application with Controller '/hello' that always returns next JSON:
 * <p>
 * {
 * "message": "Hello, ITS Partner"
 * }
 */

import java.util.HashMap;

@RestController
@RequestMapping(path = "/hello")
public class Controller {
    @GetMapping("")
    public HashMap employee() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("message", "Hello, ITS Partner");
        return map;
    }
}


