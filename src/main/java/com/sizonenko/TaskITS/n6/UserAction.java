package com.sizonenko.TaskITS.n6;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserAction {
    public List<User> removeDublicates(List<User> users) {
        Map<String, User> map = new LinkedHashMap<>();
        users.forEach(user -> map.put(user.getName(), user));
        List<User> result = new ArrayList<>(map.size());
        map.forEach((key, value) -> result.add(value));
        return result;
    }

    public void makeLowercaseEmail(List<User> users) {
        users.forEach(user -> user.setEmail(user.getEmail().toLowerCase()));
    }

    public void removeByPassword(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            String password = users.get(i).getPassword();
            if (password.length() < 8 ||
                    !password.matches(".*([#@-]).*") ||
                    !checkUpperLetter(password)) {
                users.remove(i);
                i--;
            }
        }
    }

    private boolean checkUpperLetter(String password) {
        int sum = 0;
        char[] charArray = password.toCharArray();
        for (char element : charArray) {
            if (Character.isUpperCase(element)) {
                sum++;
            }
        }
        return sum == 1;
    }
}
