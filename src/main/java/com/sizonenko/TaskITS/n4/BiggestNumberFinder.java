package com.sizonenko.TaskITS.n4;

public interface BiggestNumberFinder {

    int findTheBiggestNumber(int[] numbers);
}

