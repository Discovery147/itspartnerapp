package com.sizonenko.TaskITS.n4;

/**
 * Task:
 * 1) Implement interface BiggestNumberFinder to provide functionality of finding the biggest number from array
 * 2) Write unit tests for this class with JUnit
 */
public class MainFourth implements BiggestNumberFinder {

    public int findTheBiggestNumber(int[] numbers) {
        int sum = Integer.MIN_VALUE;
        for (int number : numbers) {
            if (number > sum) {
                sum = number;
            }
        }
        return sum;
    }
}



