package com.sizonenko.TaskITS.n6;

import java.util.ArrayList;
import java.util.List;

public class DataParser {

    private static final String DELIMETER = ",";

    public static List<User> parseDataToUser(List<String> strings) {
        List<User> users = new ArrayList<>(strings.size());
        String[] userData;
        User user;
        for (String userString : strings) {
            userData = userString.split(DELIMETER);
            user = new User(userData[0], userData[1], userData[2]);
            users.add(user);
        }
        return users;
    }

    public static List<String> parseUsersToStrng(List<User> users) {
        List<String> list = new ArrayList<>(users.size());
        users.forEach(user -> list.add(user.getName() + DELIMETER + user.getPassword() + DELIMETER + user.getEmail()));
        return list;
    }
}
